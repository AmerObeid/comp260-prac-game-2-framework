﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour {

	public float speed = 4.0f;        // metres per second
	public float turnSpeed = 180.0f;  // degrees per second
	public Transform target;
	public Vector2 heading = Vector3.right;

	private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
	}


	void Update() {

		Vector3 direction = target.position - transform.position;
		rigidbody.velocity = direction.normalized * speed;

		/*
		// get the vector from the bee to the target 
		Vector2 direction = target.position - transform.position;

		direction = direction.normalized;

		Vector2 velocity = direction * speed;
		transform.Translate (velocity*Time.deltaTime);

		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		// turn left or right
		if (direction.IsOnLeft(heading)) {
			// target on left, rotate anticlockwise
			heading = heading.Rotate(angle);
		}
		else {
			// target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}

		transform.Translate(heading * speed * Time.deltaTime);
		*/
	}
}
