﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddleKB : MonoBehaviour {

	public string horizontalAxis;
	public string verticalAxis;

		private Rigidbody rigidbody;
		public float speed = 20f;
		public float force = 10f;

	// Use this for initialization
	void Start () {
			rigidbody = GetComponent<Rigidbody>();
			rigidbody.useGravity = false;
	}
		

	// Update is called once per frame
	void Update () {
		Vector2 direction;
		direction.x = Input.GetAxis(horizontalAxis);
		direction.y = Input.GetAxis(verticalAxis);

		rigidbody.velocity = direction * speed;


	}
}

